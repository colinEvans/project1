#include <Arduino.h>

#define D13 1
#define ON 2
#define OFF 3
#define BLINK 4

#define LED 5
#define GREEN 5
#define RED 6

#define SET 7
#define STATUS 8
#define LEDS 9
#define VERSION 10
#define HELP 11

char lut[] = {
'D', '1', '3', D13,
'O', 'N', '2', ON,
'O', 'F', '3', OFF,
'B', 'L', '5', BLINK,

'L', 'E', '3', LED,
'G', 'R', '5', GREEN,
'R', 'E', '3', RED,

'S', 'E', '3', SET,
'S', 'T', '6', STATUS,
'L', 'E', '4', LEDS,
'V', 'E', '7', VERSION,
'H', 'E', '4', HELP,

'0'
};

int led1State = 0;  // 0 = off, 1 = on, 2 = blink
int led2State = 0;  // 0 = off, 1 = on, 2 = blink
int led2Color = 0;  // 0 = red, 1 = green
long int blinkDelay = 500; //number of milliseconds between each blink
char  *versionNumber = "2.0";

char sentenceBuffer[32];
int sentenceLength = 0;

long int strToInt(char str[], int length) {
	long int retVal = 0;
	if(length  < 1) return -1;

	for(int x = 0; x < length; x++) {

		char c = str[x];
		int numC = (int)c - 48;
		if( numC < 0 || numC > 9) return -1;

		long int tenPower = 1;
		for(int z = 0; z < length - 1 - x; z++) { tenPower *= 10; }
		retVal += tenPower*numC;
	}

	return retVal;
}

void executeTokens(long int tokens[], int length) {

    switch(tokens[0]) {

        case LED:
            switch(tokens[1]) {
                case GREEN:
                    led2State = 1;
                    led2Color = 1;
                break;

                case RED:
                    led2State = 1;
                    led2Color = 0;
                break;

                case OFF:
                    led2State = 0;
                break;

                case BLINK:
                    led2State = 2;
                break;
            }
        break;

        case HELP:
            Serial.println("===================================");
            Serial.println("Available Commands:");
            Serial.println("D13 + <ON, OFF, BLINK>");
            Serial.println("LED + <GREEN, RED, OFF, BLINK>");
            Serial.println("SET BLINK <0-65535 (ms)>");
            Serial.println("STATUS LEDS");
            Serial.println("VERSION");
            Serial.println("HELP");
            Serial.println("===================================");
        break;

        case VERSION:
            Serial.print("Version: ");
            Serial.println(versionNumber);
        break;

        case SET:

            switch(tokens[1]) {
                case BLINK:
                    blinkDelay = tokens[2];
                    Serial.print("Blink Time: ");
                    Serial.print(tokens[2]);
                    Serial.println(" ms.");
                break;
            }

        break;

        case D13:
            switch(tokens[1]) {
                case ON:
                    led1State = 1;
                break;

                case OFF:
                    led1State = 0;
                break;

                case BLINK:
                    led1State = 2;
                break;
            }
        break;
    }


    if(tokens[0] == STATUS && tokens[1] == LEDS) {
        if(led1State == 0) Serial.println("Built-in LED Off");
        if(led1State == 1) Serial.println("Built-in LED On");
        if(led1State == 2) Serial.println("Built-in LED Blinking");

        if(led2State == 0) Serial.println("External LED Off");
        if(led2State == 1) {
            if(led2Color == 0) Serial.println("External Led Red");
            if(led2Color == 1) Serial.println("External Led Green");
        }
        if(led2State == 2) {
            if(led2Color == 0) Serial.println("External Led Blinking Red");
            if(led2Color == 1) Serial.println("External Led Blinking Green");
        }
    } 
}

int getToken(char str[], int length) {

    int x = 0;
    while(1) {

        if(lut[x] == '0') break;

        int lutLength = (int)lut[x + 2] - 48;

        if(lut[x] == str[0] && lut[x + 1] == str[1] && length == lutLength)
            return (int)lut[x + 3];

        x += 4;
    }

    return 0;
}

void parse(char str[], int length) {


    char word[10];
    int wordLength = 0;
    int index = 0;

    long int tokens[3] = {0,0,0};;
    int tokenLength = 0;

    while(1) {

        if(str[index] == '\0' || str[index] == ' ') {

            long int wordToNum = strToInt(word, wordLength);

            if(wordLength > 0 ) {


                if(wordToNum == -1) tokens[tokenLength] = getToken(word, wordLength);
                else {
                    if(wordToNum < 0) wordToNum = 0;
                    if(wordToNum > 65535) wordToNum = 65535;
                    tokens[tokenLength] = wordToNum;
                }

                tokenLength++;
            }

            memset(word, ' ', 10*sizeof(char));
            wordLength = 0;
        }

        else if(wordLength < 10) {
           word[wordLength] = str[index];
           wordLength++;
        }

        if(str[index] == '\0' || tokenLength > 2) break;

        index++;
    }

    executeTokens(tokens, tokenLength);
}

void setup() {
  Serial.begin(9600);
  pinMode(13,OUTPUT);
  pinMode(12,OUTPUT);
  pinMode(11,OUTPUT);
}

int isCharValid(char c) {
	int a = (int)c;
	if(a == 13) return 1; //enter
	if(a == 127) return 1; //backspace
	if(a == 32) return 1; //space
	if(a > 64 && a < 91) return 1; //capital letters
	if(a > 96 && a < 123) return 1; //lower-case letters
	if(a > 47 && a < 58) return 1; //numbers
	return 0;
}

void loop()
{
	char c = Serial.read();

	if( isCharValid(c) ) {

        Serial.print(c);

        if((int)c == 127) {
            Serial.print("\b \b\b");
            sentenceLength--;
            if(sentenceLength < 0) sentenceLength = 0;
            if(sentenceLength > 31) sentenceLength = 31;
            sentenceBuffer[sentenceLength] = '0';
        } 

		else if(c != '\r' && sentenceLength < 32) {
			sentenceLength++;
			sentenceBuffer[sentenceLength - 1] = c;
		}

		else {
			Serial.println("");
			sentenceBuffer[sentenceLength] = '\0';
			parse(sentenceBuffer, sentenceLength);
			for(int x = 0; x < 32; x++){ sentenceBuffer[x] = '0'; }
			sentenceLength = 0;
		}
	}

	//Built in LED ===================================================================
	if(led1State == 0) digitalWrite(LED_BUILTIN, LOW);
	if(led1State == 1) digitalWrite(LED_BUILTIN, HIGH);
	if(led1State == 2) digitalWrite(LED_BUILTIN, (millis() / blinkDelay) % 2);

	//Red and Green LED ==============================================================
	if(led2State == 0) {
		digitalWrite(12, LOW);
		digitalWrite(11, LOW);

	}

	if(led2State == 1) {

		if(led2Color == 0) {
			digitalWrite(12, HIGH);
			digitalWrite(11, LOW);
		}

		if(led2Color == 1) {
			digitalWrite(12, LOW);
			digitalWrite(11, HIGH);
		}
	}

	if(led2State == 2) {


		int blinkState = 1;
        if(blinkDelay > 0) blinkState = (millis() / blinkDelay) % 2;


		if(led2Color == 0) {
			digitalWrite(12, blinkState);
			digitalWrite(11, LOW);
		}

		if(led2Color == 1) {
			digitalWrite(12, LOW);
			digitalWrite(11, blinkState);
		}
	}
}
